﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    private bool hasKey;
    [SerializeField] private GameObject keyTextGO;

    public void SetKey() {
        hasKey = true;
        keyTextGO.SetActive(true);
    }

    public bool HasKey(){
        return hasKey;
    }

    public void ResetKey() {
        hasKey = false;
    }
}
