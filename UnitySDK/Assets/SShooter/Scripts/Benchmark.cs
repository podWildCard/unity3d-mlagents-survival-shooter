﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Benchmark : MonoBehaviour {
    
    private static Benchmark instance;
    public static Benchmark Instance => instance;
    private SShoterStatistic statistics;
    [SerializeField] private bool recordStatistics;
    private int numberRuns = 1000;
    private DateTime benchmarkStart;
    [SerializeField] private DebugMetrics debugMetrics;

    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        } else {
            instance = this;
        }
        statistics = new SShoterStatistic{reachedLevels = new List<int>()};
        if (recordStatistics) {
            Debug.Log("start Benchmark");   
            benchmarkStart = DateTime.Now;
        }
}

    public void PrintStatistics() {
        Debug.Log("--- Statistics----------");
        Debug.Log("Shoot count: " + statistics.shoots.ToString());
        Debug.Log("Shoot accuracy: " + (float)statistics.hits / (float)statistics.shoots);
        Debug.Log("kill count: " + statistics.kills.ToString());
        Debug.Log("running Distance: " + statistics.runningDistance.ToString());
        Debug.Log("max reached level: " + statistics.reachedLevels.Max().ToString());
        Debug.Log("min reached level: " + statistics.reachedLevels.Min().ToString());
        Debug.Log("average reached level: " + statistics.reachedLevels.Average().ToString());
        Debug.Log("number runs: " + statistics.reachedLevels.Count);
        TimeSpan time = DateTime.Now - benchmarkStart;
        Debug.Log("duration: " + time.ToString(@"dd\.hh\:mm\:ss"));
    }

    public void AddShoot(){
        if (recordStatistics) {
            statistics.shoots++;
            debugMetrics.SetNewStatistics(statistics);
        }
    }
    
    public void AddHit() {
        if (recordStatistics) {
            statistics.hits++;
            debugMetrics.SetNewStatistics(statistics);
        }
    }
    
    public void AddKill() {
        if (recordStatistics) {
            statistics.kills++;
            debugMetrics.SetNewStatistics(statistics);
        }
    }
    
    public void AddDistance(float distance) {
        if (recordStatistics) {
            statistics.runningDistance += distance;
            debugMetrics.SetNewStatistics(statistics);
        }
    }
    
    public void AddLevel(int reachedLevel) {
        if (!recordStatistics) {
            return;
        }
        statistics.reachedLevels.Add(reachedLevel);
        debugMetrics.SetNewStatistics(statistics);
        Debug.Log("Round: " + statistics.reachedLevels.Count);
        if (numberRuns == statistics.reachedLevels.Count) {
            PrintStatistics();
            recordStatistics = false;
            Time.timeScale = 0f;
        }
    }

    public SShoterStatistic GetStatistics() {
        return statistics;
    }
}

public struct SShoterStatistic {
    public int shoots;
    public int hits;
    public int kills;
    public float runningDistance;
    public List<int> reachedLevels;
}