﻿using System.Collections;
using System.Collections.Generic;
using CompleteProject;
using UnityEngine;

namespace SShooter {
    public class Portal : MonoBehaviour {

        [SerializeField] private WaveManager waveManager;

        void OnCollisionEnter(Collision collision)
        {
            if (!collision.gameObject.CompareTag("Player")) {
                return;
            }

            Player player = collision.gameObject.GetComponent<Player>();
            if (player != null && waveManager.IsPortalKeyTaken()) {
                waveManager.StartNewWave();
            }
        }
    }
}