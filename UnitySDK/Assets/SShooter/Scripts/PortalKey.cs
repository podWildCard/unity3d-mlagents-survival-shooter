﻿using SShooter;
using UnityEngine;

public class PortalKey : MonoBehaviour {

    [SerializeField] private ShooterAcademy shooterAcademy;
    private WaveManager waveManager;

    void OnCollisionEnter(Collision collision) {
        if (!collision.gameObject.CompareTag("Player")) {
            return;
        }
        Player player = collision.gameObject.GetComponent<Player>();
        if (player != null) {
            if (shooterAcademy != null) {
                shooterAcademy.GetShooterAgent().AddReward(0.8f);    
                waveManager.OpenPortal();
            }
            else {
                Debug.LogError("no academy is set");
            }
            Destroy(gameObject);
        }
    }

    public void SetAcademy(ShooterAcademy academy) {
        shooterAcademy = academy;
    }

    public void SetWaveManager(WaveManager waveManager){
        this.waveManager = waveManager;
        waveManager.SetKeyActive(gameObject);
    }
}
