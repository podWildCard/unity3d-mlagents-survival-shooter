﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomObstacle : MonoBehaviour {

    [SerializeField] private BoxCollider triggerCollider;

    public BoxCollider GetCollider() {
        return triggerCollider;
    }

    public void RemoveTriggerCollider() {
        Destroy(triggerCollider);
    }
}
