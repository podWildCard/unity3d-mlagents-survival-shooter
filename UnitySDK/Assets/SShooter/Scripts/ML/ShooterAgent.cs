﻿using MLAgents;
using UnityEngine;

namespace SShooter {
    public class ShooterAgent : Agent {
        [SerializeField] private CompleteProject.PlayerShooting playerShooting;
        [SerializeField] private CompleteProject.PlayerHealth playerHealth;
        [SerializeField] private WaveManager waveManager;
        [SerializeField] private DebugMetrics debugMetrics;
        private Rigidbody agentRigidBody;
        private RayPerception3D rayPerception3D;
        private PlayerMovement playerMovement;
        private float[] rayAngles;
        private int observationClusterCount = 45;
        private int rayDistance = 14;
        private int movementSpeed = 20;
        private string[] detectableObjects = {"wallAndObstacle", "enemy", "keyAndPortal"};
        [SerializeField] private bool observationVisualisation; 
        
        public override void InitializeAgent() {
            base.InitializeAgent();
            SetObservationClusters(observationClusterCount);
            rayPerception3D = GetComponent<RayPerception3D>();
            agentRigidBody = GetComponent<Rigidbody>();
            playerMovement = GetComponent<PlayerMovement>();
            agentRigidBody.maxAngularVelocity = 500;
        }

        private void SetObservationClusters(int clusterCount) {
            rayAngles = new float[clusterCount];
            int sectorDegree = 360 / clusterCount;
            for (int i = 1; i < clusterCount; i++) {
                rayAngles[i - 1] = sectorDegree * i;
            }
        }

        public override void CollectObservations() {
            Observations observations = new Observations();
            //detectableObjects
            observations.rayObservations = rayPerception3D.Perceive(rayDistance, rayAngles, detectableObjects, 1.0f, 0f,observationVisualisation).ToArray();
            AddVectorObs(observations.rayObservations);
            // looking direction
            observations.lookingDirection = transform.rotation.eulerAngles.y; 
            AddVectorObs(observations.lookingDirection);
            //weapon rdy?
            observations.weaponState = System.Convert.ToInt32(playerShooting.IsGunLoaded());
            AddVectorObs(observations.weaponState); 
            // key (rdy, path x,path z)
            observations.keyState = waveManager.IsKeyActive(); 
            AddVectorObs(observations.keyState);
            Vector3 keyPosition = waveManager.GetKeyPosition();
            observations.keyPosition = new Vector2(observations.keyState ? keyPosition.x - transform.position.x : 0,observations.keyState ? keyPosition.z - transform.position.z : 0).normalized;
            AddVectorObs(observations.keyPosition);
            // portal (rdy, path x,path z)
            observations.portalState = waveManager.IsPortalActive();
            AddVectorObs(observations.portalState);
            Vector3 portalPosition = waveManager.GetPortalPosition();
            observations.portalPosition = new Vector2(observations.portalState ? portalPosition.x - transform.position.x : 0,observations.portalState ? portalPosition.z - transform.position.z : 0).normalized;
            AddVectorObs(observations.portalPosition);
            //player health
            observations.playerHealth = playerHealth.GetCurrentHealthInPercent();
            AddVectorObs(observations.playerHealth);
            debugMetrics.SetObservations(observations);
        }

        private void MoveAgent(float[] vectorAction) {
            int forBackMove = 0;
            int forBack = Mathf.FloorToInt(vectorAction[0]);
            switch (forBack) {
                case 1:
                    forBackMove = 1 * movementSpeed;
                    break;
                case 2:
                    forBackMove = -1 * movementSpeed;
                    break;
            }

            int leftRightMove = 0;
            int leftRight = Mathf.FloorToInt(vectorAction[1]);
            switch (leftRight) {
                case 1:
                    leftRightMove = 1 * movementSpeed;
                    break;
                case 2:
                    leftRightMove = -1 * movementSpeed;
                    break;
            }
            playerMovement.Move(leftRightMove,forBackMove);
        }

        private void Shooting(float[] vectorAction) {
            int shoot = Mathf.FloorToInt(vectorAction[2]);
            if (shoot == 1 && playerShooting.IsGunLoaded()) {
                playerShooting.Shoot();
            }
        }

        private void LookAtDirection(float[] vectorAction) {
            int targetDegree = Mathf.FloorToInt(vectorAction[3]);
            if (targetDegree == 1) {
                transform.Rotate(transform.up * -1, Time.deltaTime * 200f);
            }

            if (targetDegree == 2) {
                transform.Rotate(transform.up * +1, Time.deltaTime * 200f);
                transform.eulerAngles = new Vector3(0, transform.eulerAngles.y+1,0);
            }
        }

        public override void AgentAction(float[] vectorAction, string textAction) {
            ProcessDebaugActions(vectorAction);
            MoveAgent(vectorAction);
            Shooting(vectorAction);
            LookAtDirection(vectorAction);
            RewardFunctionTimePenalty();
            KeyDistancePenalty();
            PortalDistancePenalty();
        }

        private void ProcessDebaugActions(float[] vectorActions) {
            Actions actions = new Actions();
            actions.moveFB = vectorActions[0];
            actions.moveRL = vectorActions[1];
            actions.fire = vectorActions[2];
            actions.turn = vectorActions[3];
            debugMetrics.SetActions(actions);
        }

        public override void AgentReset() {
            oldKeyPosition = Vector3.zero;
            oldPortalPosition = Vector3.zero;
        }
        
        private void RewardFunctionTimePenalty() {
            AddReward(-0.00003f);
        }

        public Vector3 oldKeyPosition = Vector3.zero;
        private float oldKeyDistance = 0;
        private void KeyDistancePenalty() {
            if (!waveManager.IsKeyActive()) {
                return;
            }

            if (oldKeyPosition == Vector3.zero) {
                oldKeyPosition = waveManager.GetKeyPosition();
                oldKeyDistance = Vector3.Distance(oldKeyPosition, transform.position);
                return;
            }

            float newDistance = Vector3.Distance(oldKeyPosition, transform.position);
            if (newDistance > oldKeyDistance) {
                AddReward(-0.002f);
            }
            else if(newDistance < oldKeyDistance) {
                AddReward(0.008f);
            }
            oldKeyPosition = waveManager.GetKeyPosition();
            oldKeyDistance = newDistance;
        }
        
        public Vector3 oldPortalPosition = Vector3.zero;
        private float oldPortalDistance = 0;
        private void PortalDistancePenalty() {
            if (!waveManager.IsPortalActive()) {
                return;
            }
            if (oldPortalPosition == Vector3.zero) {
                oldPortalPosition = waveManager.GetPortalPosition();
                oldPortalDistance = Vector3.Distance(oldPortalPosition, transform.position);
                return;
            }

            float newDistance = Vector3.Distance(oldPortalPosition, transform.position);
            if (newDistance > oldPortalDistance) {
                AddReward(-0.002f);
            }
            else if (newDistance < oldPortalDistance){
                AddReward(0.008f);
            }
            oldPortalPosition = waveManager.GetPortalPosition();
            oldPortalDistance = newDistance;            
        } 
    }
}