﻿using System;
using UnityEngine;

public class RealPlayerComponent : MonoBehaviour {
    int floorMask;                      // A layer mask so that a ray can be cast just at gameobjects on the floor layer.
    float camRayLength = 100f;          // The length of the ray from the camera into the scene.

    void Awake() {
        floorMask = LayerMask.GetMask ("Floor");
    }

    public float GetRealPlayerMouseAngle(){
        int angle = 0;
        // Create a ray from the mouse cursor on screen in the direction of the camera.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;
        // Perform the raycast and if it hits something on the floor layer...
        if (!Physics.Raycast(camRay, out floorHit, camRayLength, floorMask)) {
            return 0;
        }
        Vector3 playerToMouse = floorHit.point - transform.position;
        playerToMouse.y = 0f;
        return FindDegree(playerToMouse.x, playerToMouse.z);
    }
    
    public static float FindDegree(double x, double y){
        double value = (Math.Atan2(x, y) / Math.PI) * 180;
        if(value < 0d) value += 360d;
        return (float)value;
    }
}
