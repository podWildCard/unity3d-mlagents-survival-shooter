﻿using MLAgents;
using UnityEngine;

namespace SShooter {
    
    public class ShooterAcademy : Academy {

        [SerializeField] private ShooterAgent sAgent;
        [SerializeField] private ShooterAgent sAgentStudent;
        [SerializeField] private bool landingView;
        [SerializeField] private WaveManager waveManager;
        [SerializeField] private CompleteProject.PlayerHealth playerHealth;
        [SerializeField] private CompleteProject.PlayerHealth playerHealthStudent;
        private Vector3 agentStartPosition;

        void Start() {
            agentStartPosition = sAgent.gameObject.transform.position;
        }

        public ShooterAgent GetShooterAgent() {
            return sAgent;
        }
        
        public ShooterAgent GetShooterAgentStudent() {
            return sAgentStudent.gameObject.activeSelf ? sAgentStudent : null;
        }

        public void Restart() {
            playerHealth.ResetHealth();
            if (sAgentStudent.gameObject.activeSelf) {
                playerHealthStudent.ResetHealth();    
            }
            waveManager.RestartWaves();
            sAgent.gameObject.transform.position = agentStartPosition;
            if (sAgentStudent.gameObject.activeSelf) {
                sAgentStudent.gameObject.transform.position = agentStartPosition;    
            }
        }
    }
}