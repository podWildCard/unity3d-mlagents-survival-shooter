﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SShooter {
public class ObstacleSpawner : MonoBehaviour {

    [SerializeField] private GameObject[] obstaclesDB;
    private List<GameObject> spawnedObstacles;
    [SerializeField] private Transform spawnTransform;
    int overflowProtection = 0;
    public float dimX;
    public float dimY;

    private void Awake() {
        spawnedObstacles = new List<GameObject>();
    }

    void Start () {
        Mesh mesh = transform.GetComponent<MeshFilter>().mesh;
        dimX = mesh.bounds.size.x*4.09f;
        dimY = mesh.bounds.size.z*3.5f;
        StartSpawning();
    }

    public void ReSpawnObstacles() {
        foreach (GameObject obstacle in spawnedObstacles) {
            Destroy(obstacle);
        }
        spawnedObstacles.Clear();
        StartSpawning();
    }
    
    private void StartSpawning() {
        Physics.autoSyncTransforms = true;
        foreach (GameObject go in obstaclesDB) {
            if (go != null) {
                Spawn(go);    
            }
        }
        Physics.autoSyncTransforms = false;
    }
 
    private void Spawn(GameObject spawnObject) {
        GameObject preflightGO = Instantiate (spawnObject, spawnTransform);
        BoxCollider boxCollider = preflightGO.GetComponent<RandomObstacle>().GetCollider();
        Vector3 halfExends = boxCollider.size/2.2f;
        Vector3 posPosition = GetRandomPosition();
        while (Hit(posPosition,halfExends,boxCollider.transform.rotation)) {
            posPosition = GetRandomPosition();
            overflowProtection++;
            if (overflowProtection > 100) {
                return;
            }
        }
        preflightGO.transform.localPosition = posPosition;
        GameObject finalGO = Instantiate(spawnObject, posPosition, boxCollider.transform.rotation, spawnTransform);
        spawnedObstacles.Add(finalGO);
        finalGO.GetComponent<RandomObstacle>().RemoveTriggerCollider();
        DestroyImmediate(preflightGO);
    }

    private bool Hit(Vector3 posPosition,Vector3 halfExends, Quaternion direction) {
        Collider[] colliders = Physics.OverlapBox(posPosition, halfExends, direction);
        return colliders.Any(collider => collider.isTrigger || collider.gameObject.name.Equals("Player") || collider.tag == "wallAndObstacle");
    }
    
    private Vector3 GetRandomPosition() {
        Vector3 position = Vector3.zero;
        position.x = Random.Range(-dimX/2f, dimX/2f);
        position.y = 0.02f;
        position.z = Random.Range(-dimY/2f, dimY/2f);
        position = Quaternion.Euler(0, 45, 0) * position;
        return position;
    }
}    
}
