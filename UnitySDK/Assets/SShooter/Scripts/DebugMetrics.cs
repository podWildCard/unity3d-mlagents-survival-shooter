﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class DebugMetrics : MonoBehaviour {

    [SerializeField] private GameObject DebugWindow;
    //Observations
    [SerializeField] private TMP_Text lookingDirection;
    [SerializeField] private TMP_Text weaponReady;
    [SerializeField] private TMP_Text keyState;
    [SerializeField] private TMP_Text keyPosition;
    [SerializeField] private TMP_Text portalState;
    [SerializeField] private TMP_Text portalPosition;
    [SerializeField] private TMP_Text playerHealth;
    //Actions
    [SerializeField] private TMP_Text[] branch0;
    [SerializeField] private TMP_Text[] branch1;
    [SerializeField] private TMP_Text[] branch2;
    [SerializeField] private TMP_Text[] branch3;
    private List<TMP_Text> allActions;
    //Statistics
    [SerializeField] private TMP_Text shoots;
    [SerializeField] private TMP_Text hits;
    [SerializeField] private TMP_Text accuracy;
    [SerializeField] private TMP_Text distance;
    [SerializeField] private TMP_Text maxLevel;
    [SerializeField] private TMP_Text kills;
    //360
    private TMP_Text[] rayobservations;
    [SerializeField] private GameObject rayObservatisonParent;
    
    
    private bool IsDebugMode { get; set; }
    private bool isGameRunning = true;

    public void SetObservations(Observations observations) {
        if (!IsDebugMode) {
            return;
        }
        lookingDirection.text = observations.lookingDirection.ToString();
        weaponReady.text = observations.weaponState.ToString();
        keyState.text = observations.keyState ? "1" : "0";
        keyPosition.text = observations.keyState ? observations.keyPosition.ToString() : "-";
        portalState.text = observations.portalState ? "1" : "0";
        portalPosition.text = observations.portalState ? observations.portalPosition.ToString() : "-";
        playerHealth.text = observations.playerHealth.ToString();
        for (int i = 0; i < observations.rayObservations.Length; i++) {
            rayobservations[i].text = observations.rayObservations[i].ToString("0.#");
        }
        
    }
    
    public void SetActions(Actions actions) {
        if (!IsDebugMode) {
            return;
        }
        AddAllActions();
        ClearActions();
        branch0[Mathf.FloorToInt(actions.moveFB)].text = "X";
        branch1[Mathf.FloorToInt(actions.moveRL)].text = "X";
        branch2[Mathf.FloorToInt(actions.fire)].text = "X";
        branch3[Mathf.FloorToInt(actions.turn)].text = "X";
    }

    public void SetNewStatistics(SShoterStatistic statistic) {
        shoots.text = statistic.shoots.ToString();
        hits.text = statistic.hits.ToString();
        accuracy.text = (((float) statistic.hits / (float) statistic.shoots)*100).ToString("0.00");
        distance.text = statistic.runningDistance.ToString("0.00");
        maxLevel.text = statistic.reachedLevels.Count == 0 ? "1" : statistic.reachedLevels.Max().ToString();
        kills.text = statistic.kills.ToString();
    }

    private void ClearActions() {
        foreach (TMP_Text tmpText in allActions) {
            tmpText.text = "";
        }
    }

    public void ToogleDebugMode() {
        IsDebugMode = !IsDebugMode;
        DebugWindow.SetActive(IsDebugMode);
        rayobservations = rayObservatisonParent.GetComponentsInChildren<TMP_Text>();
    }
    
    private void AddAllActions() {
        allActions = new List<TMP_Text>();
        allActions.AddRange(branch0.ToList());
        allActions.AddRange(branch1.ToList());
        allActions.AddRange(branch2.ToList());
        allActions.AddRange(branch3.ToList());
    }

    public void Freezegame() {
        isGameRunning = !isGameRunning;
        Time.timeScale = isGameRunning ? 1 : 0;
    }
}

public struct Observations {
    public float lookingDirection;
    public int weaponState;
    public bool keyState;
    public Vector2 keyPosition;
    public bool portalState;
    public Vector2 portalPosition;
    public float playerHealth;
    public float[] rayObservations;
}

public struct Actions {
    public float moveFB;
    public float moveRL;
    public float fire;
    public float turn;
}
