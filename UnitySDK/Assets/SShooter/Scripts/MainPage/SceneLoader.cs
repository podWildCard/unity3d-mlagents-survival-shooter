﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    public void LoadLandingViewScene() {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
    
    public void LoadKIViewScene() {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }
    
    public void LoadPlayerViewScene() {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void Exit() {
        Application.Quit();
    }
}
