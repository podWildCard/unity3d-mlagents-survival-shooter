﻿using System;
using MLAgents;
using TMPro;
using UnityEngine;

namespace SShooter {
    public class WaveManager : MonoBehaviour {

        [SerializeField] private ShooterAcademy sAcademy;
        [SerializeField] private ObstacleSpawner obstacleSpawner;
        [SerializeField] private int waveIndex = 1;
        [SerializeField] private float waveTimeInSeconds = 45;
        [SerializeField] private CompleteProject.EnemyManager enemyManager;
        [SerializeField] private TMP_Text waveIndexText;
        [SerializeField] private TMP_Text waveTimeText;
        [SerializeField] private TMP_Text nextWaveText;
        [SerializeField] private GameObject keyTakenTxtGO;
        [SerializeField] private GameObject portal;
        [SerializeField] private CompleteProject.PlayerHealth playerHealth;
        [SerializeField] private Player player;
        [SerializeField] private bool countTime = true;
        private GameObject portalKeyGO;
        private float waveTimeLeft;
        private bool isPortalKeyTaken;

        void Update() {
            if (countTime) {
                waveTimeLeft -= Time.deltaTime;    
            }
            waveTimeText.text = waveTimeLeft >= 0 ? waveTimeLeft.ToString("0.0") : "0";
            if (waveTimeLeft <= 0) {
                playerHealth.Death();
                sAcademy.GetShooterAgent().AddReward(-1);
                sAcademy.GetShooterAgent().Done();
                Agent student = sAcademy.GetShooterAgentStudent();
                if (student != null) {
                    student.Done();
                }
            }
            nextWaveText.color = Color.Lerp (nextWaveText.color, Color.clear, 1.2f * Time.deltaTime);
        }
        private void Start() {
            waveTimeLeft = waveTimeInSeconds;
            waveIndexText.text = waveIndex.ToString();
            nextWaveText.text = "Wave " + waveIndex.ToString();
            nextWaveText.color = Color.red;
            enemyManager.StartWaveSpawning(waveIndex);
        }

        public void StartNewWave() {
            // player enter portal
            sAcademy.GetShooterAgent().AddReward(1);
    
            player.ResetKey();
            keyTakenTxtGO.SetActive(false);
            if (portalKeyGO != null) {
                Destroy(portalKeyGO);    
            }
            portal.SetActive(false);
            waveTimeLeft = waveTimeInSeconds;
            waveIndex++;
            nextWaveText.color = Color.red;
            nextWaveText.text = "Wave " + waveIndex.ToString();
            waveIndexText.text = waveIndex.ToString();
            enemyManager.StartWaveSpawning(waveIndex);
        }

        public void OpenPortal() {
            portal.SetActive(true);
            isPortalKeyTaken = true;
        }

        public void SetKeyActive(GameObject keyGO) {
            portalKeyGO = keyGO;
        }

        public bool IsPortalActive() {
            return portal.activeSelf;
        }

        public Vector3 GetPortalPosition() {
            return portal.transform.position;
        }

        public bool IsKeyActive() {
            return portalKeyGO != null && portalKeyGO.activeSelf;
        }

        public Vector3 GetKeyPosition() {
            return IsKeyActive() ? portalKeyGO.transform.position : Vector3.zero;
        }

        public bool IsPortalKeyTaken() {
            return isPortalKeyTaken;
        }

        public void RestartWaves() {
            Benchmark.Instance.AddLevel(waveIndex);
            player.ResetKey();
            keyTakenTxtGO.SetActive(false);
            portal.SetActive(false);
            waveTimeLeft = waveTimeInSeconds;
            waveIndex = 1;
            waveIndexText.text = waveIndex.ToString();
            enemyManager.RemoveAllEnemies();
            enemyManager.StartWaveSpawning(waveIndex);
            Destroy(portalKeyGO);
            obstacleSpawner.ReSpawnObstacles();
            isPortalKeyTaken = false;
        }

        public void ToogleTimerMode() {
            countTime = !countTime;
        }
    }
}