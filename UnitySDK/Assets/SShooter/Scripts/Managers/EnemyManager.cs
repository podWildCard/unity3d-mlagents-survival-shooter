﻿using System;
using System.Collections.Generic;
using SShooter;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CompleteProject {
    public class EnemyManager : MonoBehaviour {
        [SerializeField] private ShooterAcademy shooterAcademy;
        public PlayerHealth playerHealth;       // Reference to the player's heatlh.
        [SerializeField] private GameObject[] normalEnemies;      // The enemy prefab to be spawned.
        [SerializeField] private GameObject bossEnemy;            // The enemy prefab to be spawned.
        [SerializeField] private float spawnTime = 1.0f;            // How long between each spawn.
        [SerializeField] private Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
        [SerializeField] private TMP_Text enemiesLeftText;
        [SerializeField] private GameObject portalKeyPrefab;
        [SerializeField] private WaveManager waveManager;
        private int waveEnemyCount = 0;
        private int spawnCount;
        private int killcount;
        private float timeSinceLastSpawn = 0;
        private List<GameObject> currentEnemies;

        private void Awake() {
            currentEnemies = new List<GameObject>();
        }

        void Update() {
            timeSinceLastSpawn += Time.deltaTime;
            if (spawnCount < waveEnemyCount && timeSinceLastSpawn >= spawnTime) {
                timeSinceLastSpawn = 0;
                Spawn(false);
            }
        }
        
        public void StartWaveSpawning(int sceneIndex) {
            killcount = 0;
            spawnCount = 0;
            waveEnemyCount = sceneIndex > 1 ? sceneIndex * 2 - 1 : 1;
            enemiesLeftText.text = waveEnemyCount.ToString();
        }
        
        private void Spawn (bool boss) {
            spawnCount++;
            // If the player has no health left...
            if(playerHealth.currentHealth <= 0f) {
                return;
            }
            // Find a random index between zero and one less than the number of spawn points.
            int spawnPointIndex = Random.Range (0, spawnPoints.Length);
            // Get the specific EnemyType
            GameObject enemyType = boss ? bossEnemy : normalEnemies[Random.Range(0, normalEnemies.Length)];
            // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
            GameObject enemy = Instantiate (enemyType, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
            currentEnemies.Add(enemy);
            EnemyHealth enemyHealth = enemy.GetComponent<EnemyHealth>();
            if (enemyHealth != null) {
                enemyHealth.enemyManager = this;
            }
        }

        public void EnemyDown(Transform enemyTransform) {
            killcount++;
            enemiesLeftText.text = (waveEnemyCount-killcount).ToString();
            if (killcount == waveEnemyCount) {
                DropPortalKey(enemyTransform);
            }
            currentEnemies.RemoveAll(item => item == enemyTransform.gameObject);
        }

        private void DropPortalKey(Transform position) {
            GameObject portalKeyGo = Instantiate(portalKeyPrefab, position.position, position.rotation);
            PortalKey portalKey = portalKeyGo.GetComponent<PortalKey>();
            portalKey.SetAcademy(shooterAcademy);
            portalKey.SetWaveManager(waveManager);
        }

        public void SetKillReward() {
            shooterAcademy.GetShooterAgent().AddReward(0.8f);
        }

        public void RemoveAllEnemies() {
            killcount = waveEnemyCount;
            foreach (GameObject enemyGO in currentEnemies) {
                Destroy(enemyGO);
            }
            currentEnemies.Clear();
        }
    }
}